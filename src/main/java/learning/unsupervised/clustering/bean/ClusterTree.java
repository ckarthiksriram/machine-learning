package learning.unsupervised.clustering.bean;

import java.util.ArrayList;
import java.util.HashMap;

/**
 * Created with IntelliJ IDEA.
 * User: prashanth.v
 * Date: 7/3/13
 * Time: 12:55 AM
 * To change this template use File | Settings | File Templates.
 */
public class ClusterTree {
    int idx;
    ClusterTree lChild;
    ClusterTree rChild;


    public ClusterTree(int idx) {
        this.idx = idx;
        this.lChild = null;
        this.rChild = null;
    }

    public int getIdx() {
        return idx;
    }

    public void setIdx(int idx) {
        this.idx = idx;
    }

    public ClusterTree getlChild() {
        return lChild;
    }

    public void setlChild(ClusterTree lChild) {
        this.lChild = lChild;
    }

    public ClusterTree getrChild() {
        return rChild;
    }

    public void setrChild(ClusterTree rChild) {
        this.rChild = rChild;
    }
    public void buildTree(ArrayList<Pair<Integer,Integer>> pairs,int n){
        HashMap<Integer,ClusterTree> tree=new HashMap<Integer, ClusterTree>();

        for(Pair<Integer,Integer> pair:pairs){

            int l=pair.getFirst();
            ClusterTree newNode=new ClusterTree(l);
            int r=pair.getSecond();
            ClusterTree lChild=tree.get(l);
            ClusterTree rChild=tree.get(r);
            System.out.println(l);
            if(lChild==null){
                lChild=new ClusterTree(l);
            }
            if(rChild==null){
                rChild=new ClusterTree(r);
            }
            else{
                tree.remove(r);
            }
            newNode.setlChild(lChild);
            newNode.setrChild(rChild);
            tree.put(l,newNode);
        }
        for(Integer x:tree.keySet()){
            System.out.println(tree.get(x));
        }

    }

    @Override
    public String toString() {
        return
                "{lChild=" + lChild +
                ", rChild=" + rChild +
                ", idx=" + idx +
                '}';
    }
}
